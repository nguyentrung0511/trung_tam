<?php
include ("header.php");
?>
<!-- Content -->
<div class="page_content_wrap">
    <div class="content">
        <article class="post_item post_item_single page type-page">
            <section class="post_content">
                <div class="sc_section" data-animation="animated fadeInUp normal">
                    <div class="sc_content content_wrap margin_top_3em_imp margin_bottom_3em_imp">
                        <h2 class="sc_title sc_title_regular sc_align_center margin_top_0 margin_bottom_085em text_center">Danh sách giảng viên </h2>
                        <div class="sc_team sc_team_style_1" data-animation="animated fadeInUp normal">

                            <div class="sc_columns columns_wrap">
                                <?php
                                foreach ($giang_vien as $gv){

                                ?>
                                <div class="column-1_3">
                                    <div class="sc_team_item sc_team_item_1 odd first">
                                        <div class="sc_team_item_avatar">
                                            <img alt="" src="public/layout/imggiangvien/<?php echo $gv->hinh_anh ?>" style="width: 400px; height: 300px"></div>
                                        <div class="sc_team_item_info">
                                            <h6 class="sc_team_item_title">
                                                <a href=""><?php echo $gv->ten_giang_vien ?></a>
                                            </h6>
                                            <div class="sc_team_item_position"><?php echo $gv->thong_tin_ca_nhan ?></div>
                                            <div class="sc_socials sc_socials_size_small">
                                                <div class="sc_socials_item">
                                                    <a href="#" target="_blank" class="social_icons social_facebook">
                                                        <span class="sc_socials_hover social_facebook"></span>
                                                    </a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a href="#" target="_blank" class="social_icons social_pinterest">
                                                        <span class="sc_socials_hover social_pinterest"></span>
                                                    </a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a href="#" target="_blank" class="social_icons social_twitter">
                                                        <span class="sc_socials_hover social_twitter"></span>
                                                    </a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a href="#" target="_blank" class="social_icons social_gplus">
                                                        <span class="sc_socials_hover social_gplus"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <?php
                                }
                                ?>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="sc_line sc_line_style_solid margin_top_0 margin_bottom_0"></div>
            </section>
        </article>
    </div>
</div>
<!-- /Content -->
<?php
include ("footer.php");
?>