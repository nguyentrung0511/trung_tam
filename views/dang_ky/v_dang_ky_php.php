<!doctype html>
<html class="no-js" lang="">


<!-- Mirrored from affixtheme.com/html/xmee/demo/login-5.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 29 Jun 2021 05:58:46 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Xmee | Login and Register Form Html Templates</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="public/register/css/bootstrap.min.css">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="public/register/css/fontawesome-all.min.css">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="public/register/font/flaticon.css">
    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="public/register/style.css">
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div id="wrapper" class="wrapper">
    <div class="fxt-template-animation fxt-template-layout6">
        <div class="fxt-intro">
            <div class="sub-title"><h3>Welcome To</h3></div>
        </div>
        <div class="fxt-bg-color">
            <div class="fxt-form">
                <form method="POST">
                    <div class="form-group fxt-transformY-60 fxt-transition-delay-1" style="width: 600px;">
                        <input type="text" class="form-control" name="ho_ten" placeholder="Họ và tên" required="required">
                        <i class="flaticon-user"></i>
                    </div>
                    <div class="form-group fxt-transformY-60 fxt-transition-delay-2">
                        <input type="email" class="form-control" name="email" placeholder="Địa chỉ gmail" required="required">
                        <i class="flaticon-envelope"></i>
                    </div>
                    <div class="form-group fxt-transformY-60 fxt-transition-delay-2">
                        <input type="text" maxlength="10" class="form-control" name="so_dien_thoai" placeholder="Số điện thoại" required="required">
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-2">
                        <label class="form-control">Học phí: <?php echo number_format($show_kh->hoc_phi)?></label>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-2">
                        <label class="form-control">Lớp học: <?php echo $show_kh->ten_khoa_hoc?></label>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-2">
                        <label class="form-control">Lớp học: <?php echo $show_kh->ten_lop_hoc?></label>
                    </div>

                    <div class="form-group fxt-transformY-50 fxt-transition-delay-2">
                        <label class="form-control">Ca học: <?php echo $show_kh->ca_hoc?></label>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-2">
                        <label class="form-control">Địa điểm học: <?php echo $show_kh->dia_diem_hoc?></label>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-2">
                        <label class="form-control">Giảng viên: <?php echo $show_kh->ten_giang_vien?></label>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-2">
                        <label for="form-control">Khuyến mại</label>
                        <select class="form-control" name="id_khuyen_mai">
                            <?php
                            foreach ($show_km as $km) {
                                ?>
                                <option value="<?php echo  $km->id;?>"><?php echo $km->ten_khuyen_mai ;?>: Giảm giá <?php echo $km->phan_tram_giam_gia ;?>%</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-3">
                        <div class="fxt-content-between">
                            <button type="submit" name="btn_save" class="fxt-btn-fill">Đăng ký</button>
                            <button name="btn_save" onclick="window.location.href='index.php'" class="fxt-btn-fill">Quay lại trang chủ</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- jquery-->
<script src="public/register/js/jquery-3.5.0.min.js"></script>
<!-- Popper js -->
<script src="public/register/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="public/register/js/bootstrap.min.js"></script>
<!-- Imagesloaded js -->
<script src="public/register/js/imagesloaded.pkgd.min.js"></script>
<!-- Validator js -->
<script src="public/register/js/validator.min.js"></script>
<!-- Custom Js -->
<script src="public/register/js/main.js"></script>

</body>


<!-- Mirrored from affixtheme.com/html/xmee/demo/login-5.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 29 Jun 2021 05:58:47 GMT -->
</html><?php
