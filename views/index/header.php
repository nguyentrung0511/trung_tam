<body class="home page body_style_fullscreen body_filled article_style_stretch layout_single-standard top_panel_style_dark top_panel_opacity_transparent top_panel_show top_panel_over menu_right user_menu_show sidebar_hide">
<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-angle-double-up" data-url="" data-separator="yes"></a>
<!-- Body -->
<div class="body_wrap">
    <div class="page_wrap">
        <div class="top_panel_fixed_wrap"></div>
        <header class="top_panel_wrap bg_tint_dark">
            <!-- Main menu -->
            <div class="menu_main_wrap logo_left">
                <div class="content_wrap clearfix">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="index-2.html">
                            <img src="public/layout/images/logo_dark.png" class="logo_main" alt="">
                            <img src="public/layout/images/logo_dark.png" class="logo_fixed" alt="">
                        </a>
                    </div>
                    <!-- Logo -->
                    <!-- Navigation -->
                    <a href="#" class="menu_main_responsive_button icon-menu-1"></a>
                    <nav class="menu_main_nav_area">
                        <ul id="menu_main" class="menu_main_nav">
                            <li class="menu-item menu-item-has-children current-menu-ancestor current-menu-parent"><a href="index.php">Trang chủ</a></li>
                            <li class="menu-item menu-item-has-children"><a href="">Danh mục khóa học</a>
                                <ul class="sub-menu">
                                    <?php
                                    foreach ($dm as $kh){
                                    ?>
                                    <li class="menu-item"><a href="khoa_hoc.php?id_dmkh=<?php echo $kh->id?>"><?php echo $kh->ten_danh_muc?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-has-children"><a href="giang_vien.php">Giảng viên</a></li>
                            <li class="menu-item menu-item-has-children"><a href="">Danh mục tin tức</a>
                                <ul class="sub-menu">
                                    <?php
                                    foreach ($km as $ki){
                                    ?>
                                    <li class="menu-item"><a href="tin_tuc.php?id_dmtt=<?php echo $ki->id?>"><?php echo $ki->ten_danh_muc?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- /Navigation -->
                </div>
            </div>
            <!-- /Main menu -->
        </header>
        <section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_education_home_slider">
            <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
                <div id="rev_slider_1_1" class="rev_slider fullwidthabanner disp_none height_900 max-height_900">
                    <ul>
                        <!-- Slide 1 -->
                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                            <img src="public/layout/images/green.jpg" alt="green" data-bgposition="center top" data-bgfit="normal" data-bgrepeat="repeat">
                            <div class="tp-caption customin stl cust-z-index-5 rs-cust-style8" data-x="20" data-y="230" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:20;transformOrigin:50% 100%;" data-speed="1300" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8250" data-endspeed="300">
                                <img src="public/layout/images/13.jpg" alt="">
                            </div>
                            <div class="tp-caption title sfr stl tp-resizeme cust-z-index-6 rs-cust-style1" data-x="570" data-y="190" data-speed="500" data-start="1350" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8400" data-endspeed="300">Tham gia các khóa học tuyệt vời từ trung tâm tốt nhất Việt Nam
                            </div>
                        </li>
                        <!-- Slide 2 -->
                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                            <img src="public/layout/images/blue.jpg" alt="blue" data-bgposition="center top" data-bgfit="normal" data-bgrepeat="repeat">
                            <div class="tp-caption customin stl cust-z-index-5 rs-cust-style8" data-x="40" data-y="200" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:360;scaleX:0.1;scaleY:0.1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" data-speed="1300" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8250" data-endspeed="300">
                                <img src="public/layout/images/slide-2-1.png" alt="">
                            </div>
                            <div class="tp-caption title sfb stb tp-resizeme cust-z-index-6 rs-cust-style1" data-x="570" data-y="200" data-speed="500" data-start="1350" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8400" data-endspeed="300">Các khóa học lập trình của chúng tôi được thiết kế theo yêu cầu của doanh nghiệp
                            </div>

                        </li>
                        <!-- Slide 3 -->
                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                            <img src="public/layout/images/yellow.jpg" alt="yellow" data-bgposition="center top" data-bgfit="normal" data-bgrepeat="repeat">
                            <div class="tp-caption roundedimage sfl stl cust-z-index-5 rs-cust-style8" data-x="50" data-y="200" data-speed="1300" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8250" data-endspeed="300">
                                <img src="public/layout/images/slide-3-1.jpg" alt="">
                            </div>
                            <div class="tp-caption title customin stb tp-resizeme cust-z-index-6 rs-cust-style1" data-x="570" data-y="200" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:20;transformOrigin:50% 100%;" data-speed="500" data-start="1350" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8400" data-endspeed="300">Tự tin đi làm sau 4-6 tháng
                            </div>
                        </li>
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
            </div>
        </section>
