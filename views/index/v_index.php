<?php
include ("header.php");
?>
        <!-- Revolution slider -->

        <!-- Revolution slider -->
        <!-- Content -->
        <div class="page_content_wrap">
            <div class="content">
                <article class="post_item post_item_single page">
                    <section class="post_content">
                        <!-- Courses section -->
                        <div class="sc_section accent_top bg_tint_light" data-animation="animated fadeInUp normal">
                            <div class="sc_section_overlay">
                                <div class="sc_section_content">
                                    <div class="sc_content content_wrap margin_top_2_5em_imp margin_bottom_2_5em_imp">
                                        <h2 class="sc_title sc_title_regular sc_align_center margin_top_0 margin_bottom_085em text_center">Danh mục khóa học</h2>
                                        <div class="sc_blogger layout_courses_2 template_portfolio sc_blogger_horizontal no_description">
                                            <div class="isotope_wrap" data-columns="3">
                                                <!-- Courses item -->
                                                <?php
                                                foreach ($dm as $kh){
                                                ?>
                                                <div class="isotope_item isotope_item_courses isotope_column_3">
                                                    <div class="post_item post_item_courses odd">
                                                        <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                                            <div class="post_featured img">
                                                                <a href="paid-course.html">
                                                                    <img alt="Principles of Written English, Part 2" src="public/layout/images/masonry_15-400x400.jpg">
                                                                </a>
                                                                <div class="post_descr">
                                                                    <div class="post_category">
                                                                        <a href="khoa_hoc.php?id_dmkh=<?php echo $kh->id?>"><?php echo $kh->ten_danh_muc?></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="post_info_wrap info">
                                                                <div class="info-back">
                                                                    <h4 class="post_title">
                                                                        <a href=""><?php echo $kh->ten_danh_muc?></a>
                                                                    </h4>
                                                                    <div class="post_descr">
                                                                        <p>
                                                                            <a href=""></a>
                                                                        </p>
                                                                        <div class="post_buttons">
                                                                            <div class="post_button">
                                                                                <a href="khoa_hoc.php?id_dmkh=<?php echo $kh->id?>" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">Chi tiết các khóa học</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /Courses item -->
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Courses section -->>
                    </section>
                </article>
            </div>
        </div>
        <!-- /Content without sidebar -->
        <?php
        include ("footer.php");
        ?>
    </div>
</div>