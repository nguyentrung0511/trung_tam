<body class="single single-post postid-92 body_style_wide body_filled article_style_boxed layout_single-standard top_panel_style_dark top_panel_opacity_solid top_panel_above menu_right sidebar_show sidebar_right">
<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-angle-double-up" data-url="" data-separator="yes"></a>
<div class="body_wrap">
    <div class="page_wrap">
        <div class="top_panel_fixed_wrap"></div>
        <header class="top_panel_wrap bg_tint_light">>
            <!-- Main menu -->
            <div class="menu_main_wrap logo_left">
                <div class="content_wrap clearfix">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="index-2.html">
                            <img src="images/logo_light.png" class="logo_main" alt="">
                            <img src="images/logo_light.png" class="logo_fixed" alt="">
                        </a>
                    </div>
                    <!-- Logo -->
                    <!-- Navigation -->
                    <a href="#" class="menu_main_responsive_button icon-menu-1"></a>
                    <nav class="menu_main_nav_area">
                        <ul id="menu_main" class="menu_main_nav">
                            <li class="menu-item menu-item-has-children"><a href="index.php">Trang chủ</a></li>
                            <li class="menu-item menu-item-has-children "><a href="">Danh mục khóa học</a>
                                <ul class="sub-menu">
                                    <?php
                                    foreach ($dm as $kh){
                                        ?>
                                        <li class="menu-item"><a href="khoa_hoc.php?id_dmkh=<?php echo $kh->id?>"><?php echo $kh->ten_danh_muc?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-has-children"><a href="giang_vien.php">Giảng viên</a></li>
                            <li class="menu-item menu-item-has-children current-menu-ancestor current-menu-parent"><a href="">Danh mục tin tức</a>
                                <ul class="sub-menu">
                                    <?php
                                    foreach ($km as $ki){
                                        ?>
                                        <li class="menu-item menu-item-has-children"><a href="tin_tuc.php?id_dmtt=<?php echo $ki->id?>"><?php echo $ki->ten_danh_muc?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- /Navigation -->
                </div>
            </div>
            <!-- /Main menu -->
        </header>