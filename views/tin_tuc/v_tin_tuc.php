<?php
include ("header.php");
?>
<div class="page_top_wrap page_top_title page_top_breadcrumbs">
    <div class="content_wrap">
        <h3 class="page_title">Danh mục tin tức: <?php echo $show_tt->ten_danh_muc ;?></h3>
    </div>
</div>

<!-- Content with sidebar -->
<div class="page_content_wrap">
    <div class="content_wrap">
        <div class="content">
            <?php
            foreach ( $show_tin_tuc as $tt){
            ?>
            <article class="post_item post_item_excerpt odd post">

                <div class="post_featured">
                    <div class="post_thumb" data-image="" data-title="Medical Chemistry: The Molecular Basis">
                        <a class="hover_icon hover_icon_link" href="">
                            <img alt="" src="public/layout/imgtintuc/<?php echo $tt->hinh_anh ;?>"></a>
                    </div>
                </div>
                <div class="post_content clearfix">

                    <h3 class="post_title">
                        <a href=""><span class="post_icon icon-book-2"></span><?php echo $tt->ten_tin_tuc ;?></a>
                    </h3>
                    <div class="post_info">
                                    <span class="post_info_item post_info_posted">Posted:
										<a href="#" class="post_info_date"><?php echo $tt->ngay_tao ;?></a>
									</span>
                    </div>
                    <div class="post_descr">
                        <p><?php echo $tt->noi_dung ;?></p>
                        <a href="chi_tiet_tin_tuc.php?id=<?php echo $tt->id ;?>" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">Chi tiết</a>
                    </div>
                </div>

            </article>
                <?php
            }
            ?>
            <nav id="pagination" class="pagination_wrap pagination_pages">
                <span class="pager_current active ">1</span>
                <a href="#">2</a>
                <a href="#" class="pager_next"></a>
                <a href="#" class="pager_last"></a>
            </nav>
        </div>
        <!-- /Content-->
    </div>
</div>
<?php
include ("footer.php");
?>
<!-- /Content with sidebar -->