<?php
include ("header_chi_tiet.php");
?>
<div class="page_top_wrap page_top_title page_top_breadcrumbs">
    <div class="content_wrap">
        <h1 class="page_title"><?php echo $show_tin_tuc->ten_tin_tuc ;?></h1>
    </div>
</div>
<div class="page_content_wrap">
    <div class="content_wrap">
        <!-- Content -->
        <div class="content">
            <article class="post_item post_item_single post">
                <section class="post_featured">
                    <div class="post_thumb" data-image="images/masonry_01.jpg" data-title="Medical Chemistry: The Molecular Basis">
                        <a class="hover_icon hover_icon_view" href="" title="">
                            <img alt="Medical Chemistry: The Molecular Basis" src="public/layout/imgtintuc/<?php echo $show_tin_tuc->hinh_anh ;?>">
                        </a>
                    </div>
                </section>
                <div class="post_info">
                                <span class="post_info_item post_info_posted">Posted
									<a href="#" class="post_info_date date updated" content="2015-01-14"><?php echo $show_tin_tuc->ngay_tao ;?></a>
								</span>
                </div>
                <section class="post_content">
                    <p><?php echo $show_tin_tuc->noi_dung ;?></p>
                </section>
            </article>
        </div>
    </div>
<?php
include ("footer.php");
?>