<!-- Contacts Footer  -->
<footer class="contacts_wrap bg_tint_dark contacts_style_dark">
    <div class="content_wrap">
        <div class="logo">
            <a href="index-2.html">
                <img src="images/logo_footer.png" alt="">
            </a>
        </div>
        <div class="contacts_address">
            <?php
            foreach ($lh as $lhe){
                ?>
                <address class="address_right">
                    Số điện thoai: <?php echo $lhe->phone?><br>
                    Gmail: <?php echo $lhe->email?>
                </address>
                <address class="address_left">
                    Địa chỉ: <?php echo $lhe->dia_chi?>
                </address>
                <?php
            }
            ?>
        </div>
        <div class="sc_socials sc_socials_size_big">
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_facebook">
                    <span class="sc_socials_hover social_facebook"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_pinterest">
                    <span class="sc_socials_hover social_pinterest"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_twitter">
                    <span class="sc_socials_hover social_twitter"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_gplus">
                    <span class="sc_socials_hover social_gplus"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_rss">
                    <span class="sc_socials_hover social_rss"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_dribbble">
                    <span class="sc_socials_hover social_dribbble"></span>
                </a>
            </div>
        </div>
    </div>
</footer>
</div>
</div>
<!-- /Contacts Footer --><!-- /Contacts Footer -->