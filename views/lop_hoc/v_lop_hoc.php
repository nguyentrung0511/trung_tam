<?php
include ("header.php");
?>
<!-- Page title -->
<div class="page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1">
    <div class="content_wrap">
        <h3 class="page_title">Khóa học: <?php echo $show_kh->ten_khoa_hoc?></h3>
    </div>
</div>
<!-- /Page title -->
<!-- Content with sidebar -->
<div class="page_content_wrap">

    <div class="content_wrap">
        <div class="content">
            <div class="content">
                <article class="post_item post_item_single page">
                    <section class="post_content">
                        <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_2">
                            <div class="column-1_2 sc_column_item sc_column_item_1 odd first">
                                <strong>Thông tin khóa học</strong>
                                <br /><?php echo $show_kh->thong_tin?>
                            </div>
                            <div class="column-1_2 sc_column_item sc_column_item_2 even">
                                <strong>Kế hoạch đào tạo</strong>
                                <br /><?php echo $show_kh->ke_hoach_dao_tao?>
                            </div>
                        </div>
                        <!-- /Columns -->
                        <div class="sc_section" data-animation="animated fadeInUp normal">
                            <div class="sc_line sc_line_style_solid"></div>
                            <h3>Các lớp học đang giảng dạy</h3>
                            <div class="sc_table width_100per">
                                <table>
                                    <tbody>
                                    <tr>
                                        <th class="width_10per">STT</th>
                                        <th>Tên lớp</th>
                                        <th>Ca học</th>
                                        <th>Thời gian bắt đầu</th>
                                        <th>Địa điểm</th>
                                        <th>Số chỗ</th>
                                        <th>Giảng viên</th>
                                        <th>Hoạt động</th>
                                    </tr>
                                    <?php
                                    $i=1;
                                    $date = date('Y-m-d');
                                    foreach ($show_lp as $lp){
                                        if(strtotime($lp->thoi_gian_bat_dau) > strtotime ( $date ) && $lp->so_cho>0 && $lp->trang_thai=1){


                                    ?>
                                    <tr class="text_center">
                                        <td><?php echo $i++  ?></td>
                                        <td><?php echo $lp->ten_lop_hoc?></td>
                                        <td><?php echo $lp->ca_hoc?></td>
                                        <td><?php echo date("d/m/Y",strtotime($lp->thoi_gian_bat_dau))?></td>
                                        <td><?php echo $lp->dia_diem_hoc?></td>
                                        <td><?php echo $lp->so_cho?></td>
                                        <td><?php echo $lp->ten_giang_vien?></td>
                                        <td> <button type="button" onclick="window.location.href='dang_ky.php?id=<?php echo $lp->id; ?>'">Đăng ký</button></td>
                                    </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </article>
            </div>
            <!-- /Courses item -->
        </div>
        </div>
    </div>

</div>
</div>
<?php
include ("footer.php");
?>

<!-- /Content -->