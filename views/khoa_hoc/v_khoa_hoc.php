<?php
include ("header.php");
?>
<!-- Page title -->
<div class="page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1">
    <div class="content_wrap">
        <h1 class="page_title"><?php echo $show_dmkh->ten_danh_muc;?></h1>
    </div>
</div>
<!-- /Page title -->
<!-- Content with sidebar -->
<div class="page_content_wrap">
    <?php

    ?>
    <div class="content_wrap">
        <div class="content">
            <div class="isotope_wrap" data-columns="3">
                <!-- Courses item -->

                <div class="page_content_wrap">
                    <div class="content_wrap">
                        <div class="content">
                            <div class="isotope_wrap" data-columns="3">
                                <!-- Courses item -->
                                <?php
                                foreach ( $show_kh as $ks){

                                    ?>
                                    <div class="isotope_item isotope_item_courses isotope_column_3">
                                        <div class="post_item post_item_courses odd">
                                            <div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
                                                <div class="post_featured img">
                                                    <a href="paid-course.html">
                                                        <img alt="Principles of Written English, Part 2" src="public/layout/imgkhoahoc/<?php echo $ks->hinh_anh?>"></a>
                                                    <h4 class="post_title">
                                                        <a href=""><?php echo $ks->ten_khoa_hoc?></a>
                                                    </h4>
                                                    <div class="post_descr">
                                                        <div class="post_price">
                                                            <span class="post_price_value"><?php echo number_format($ks->hoc_phi);?>VND</span>
                                                            <span class="post_price_period"><?php echo $ks->thoi_gian;?> Tháng</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post_info_wrap info">
                                                    <div class="info-back">
                                                        <h4 class="post_title">
                                                            <a href="paid-course.html"><?php echo $ks->ten_khoa_hoc?></a>
                                                        </h4>
                                                        <div class="post_descr">
                                                            <p>
                                                                <a href="paid-course.html"><?php echo $ks->thong_tin?></a>
                                                            </p>
                                                            <div class="post_buttons">
                                                                <div class="post_button">
                                                                    <a href="lop_hoc.php?id_kh=<?php echo $ks->id; ?>" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">Chi tiết</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <!-- /Courses item -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <!-- /Courses item -->

            </div>
        </div>

    </div>
</div>
<?php
include ("footer.php");
?>

<!-- /Content -->