<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Thêm Danh Mục Tin Tức</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="add_danh_muc_tin_tuc.php">
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputProvince">Tên Danh Mục</label>
                    <input type="text" class="form-control" name="ten_danh_muc" autofocus="true" autocomplete="on" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                        <option value="0">Không Hoạt Đông</option>
                        <option value="1">Hoạt Đông</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>