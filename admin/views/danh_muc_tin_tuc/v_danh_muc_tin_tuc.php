<!-- Main Sidebar Container -->
<?php
include ("aside.php");
include ("nav.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title"></h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title m-b-0"><a href="add_danh_muc_tin_tuc.php"><button type="button" class="btn btn-success btn-sm">Thêm danh mục tin tức</button></a></h5>
                        </div>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Tên Danh Mục</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Hoạt Động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $show_dmtt as $dmtt){

                            ?>
                            <tr>
                                <th><?php echo $i++ ;?></th>
                                <td><a href="tin_tuc.php?id_dmtt=<?php echo $dmtt->id?>"><?php echo $dmtt->ten_danh_muc ;?></a></td>
                                <td><?php echo $dmtt->trang_thai ;?></td>
                                <td><button type="button" class="btn btn-warning" onclick="window.location.href='edit_danh_muc_tin_tuc.php?id=<?php echo $dmtt->id; ?>'"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger" onclick="window.location.href='delete_danh_muc_tin_tuc.php?id=<?php echo $dmtt->id; ?>'"><i class="nav-icon fa fa-trash-alt"></i></button>
                                </td>

                            </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
