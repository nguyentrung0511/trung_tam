<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Thêm Khóa Học</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form enctype="multipart/form-data" method="post" >
            <div class="card-body">
                <div class="form-group" >
                    <label for="exampleInputProvince">Tên Tiêu Đề</label>
                    <input type="text" class="form-control" name="ten_tieu_de" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Chọn Ảnh</label>
                    <div class="col-md-12">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="f_hinh_anh" name="f_hinh_anh" required>
                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                        <option value="0">Không Hoạt Đông</option>
                        <option value="1">Hoạt Đông</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>