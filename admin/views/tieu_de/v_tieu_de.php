<!-- Main Sidebar Container -->
<?php
include ("nav.php");
include ("aside.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <div class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title m-b-0"><a href="add_tieu_de.php"><button type="button" class="btn btn-success btn-sm">Thêm tiêu đề</button></a></h5>
                            <div class="card-tools">
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Số thứ tự</th>
                                <th scope="col">Tên tiêu đề</th>
                                <th scope="col">Hình ảnh</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Hoạt động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $show_all as $kh){

                            ?>
                            <tr>
                                <th><?php echo $i++ ;?></a></th>
                                <td ><?php echo $kh->ten_tieu_de ;?></td>
                                <td><img src="../public/layout/imgtieude/<?php echo  $kh->hinh_anh?>" style="width: 80px;"></td>
                                <td>
                                    <?php if($kh->trang_thai==1){?>
                                        <span class="badge badge-pill badge-info float">Hoạt Động</span>
                                    <?php } else if ($kh->trang_thai==0){?>
                                        <span class="badge badge-pill badge-warning float">Không Hoạt Động</span>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td><button type="button" class="btn btn-warning" onclick="window.location.href='edit_tieu_de.php?id=<?php echo $kh->id; ?>'"><i class="fa fa-edit"></i></button>
                                </td>
                                <td>
                            </tr>
                            </tbody>
                            <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
