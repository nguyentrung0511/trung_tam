<!-- Main Sidebar Container -->
<?php
include ("nav.php");
include ("aside.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <?php
            if(isset($_GET['id_dmkh'])){

                ?>
                <div class="col-12">
                    <div class="callout callout-info">
                        <h5>Danh mục:
                            <?php
                            foreach ( $show_tendm as $dm) {
                                echo $dm->ten_danh_muc;
                            }
                            ?>
                        </h5>
                    </div>
                </div>
                <?php
            }else{
                ?>
                <div class="col-12 d-flex no-block align-items-center">
                    <h1 class="page-title">
                    </h1>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title m-b-0"><a href="add_khoa_hoc.php"><button type="button" class="btn btn-success btn-sm">Add New</button></a></h5>
                            <div class="card-tools">
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Số thứ tự</th>
                                <th scope="col">Tên khóa học</th>
                                <th scope="col">Thời gian khóa học</th>
                                <th scope="col">Học phí/khóa</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Hoạt động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $show_kh as $kh){

                            ?>
                            <tr>
                                <th><?php echo $i++ ;?></a></th>
                                <td><a href="lop_hoc.php?id_kh=<?php echo $kh->id;?>"><?php echo $kh->ten_khoa_hoc ;?></a></td>
                                <td><?php echo $kh->thoi_gian ;?> tháng</td>
                                <td><?php echo number_format($kh->hoc_phi) ?> VND</td>
                                <td>
                                    <?php if($kh->trang_thai==1){?>
                                        <span class="badge badge-pill badge-info float">Hoạt Động</span>
                                    <?php } else if ($kh->trang_thai==0){?>
                                        <span class="badge badge-pill badge-warning float">Không Hoạt Động</span>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-outline-info" onclick="window.location.href='details_khoa_hoc.php?id=<?php echo $kh->id; ?>'"><i class="fa fa-bars"></i></button>
                                </td>
                                <td>
                            </tr>
                            </tbody>
                            <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
