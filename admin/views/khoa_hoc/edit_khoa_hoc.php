<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Sửa Thông Tin</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form enctype="multipart/form-data" method="post" >
            <div class="card-body">
                <div class="form-group" >
                    <label for="exampleInputProvince">Tên khóa học</label>
                    <input type="text" class="form-control" name="ten_khoa_hoc" value="<?php echo $kh->ten_khoa_hoc;?>" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Thời gian</label>
                    <input type="number" min="6" class="form-control" name="thoi_gian" value="<?php echo $kh->thoi_gian;?>"  autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Thông tin khóa học</label>
                    <textarea name="thong_tin" class="form-control" required><?php echo $kh->thong_tin;?> </textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Kế Hoạch Đào Tạo</label>
                    <textarea name="ke_hoach_dao_tao" class="form-control" required><?php echo $kh->ke_hoach_dao_tao;?></textarea>
                </div>
                <div class="form-group row">
                    <label for="exampleInputProvince">Chọn Ảnh</label>
                    <div class="col-md-12">
                        <div class="custom-file" style="height: 120px;">
                            <input type="file" class="custom-file-input" id="f_hinh_anh" name="f_hinh_anh"  accept="image/*" />
                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                            <img src="../public/layout/imgkhoahoc/<?php echo $kh->hinh_anh;?>" width="120px" height="100px" >

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Học Phí Khóa Học</label>
                    <input type="number" min="1000000" class="form-control" value="<?php echo $kh->hoc_phi;?>" name="hoc_phi" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <div class="col-sm-12">
                        <select class="form-control" name="trang_thai" val>
                            <option value="1" <?php if($kh->trang_thai ==1) echo"selected";?>>Hoạt Động</option>
                            <option value="0" <?php  if($kh->trang_thai ==0) echo"selected";?>>Không Hoạt Động</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Danh Mục Khóa Học</label>
                    <select class="form-control" name="id_danh_muc">
                        <?php
                        foreach ($show_all as $dmkh) {
                            ?>
                            <option value="<?php echo $dmkh->id;?>"><?php echo $dmkh->ten_danh_muc;?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>