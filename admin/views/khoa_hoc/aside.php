<aside class="main-sidebar elevation-4 sidebar-light-info">
    <a href="index3.html" class="brand-link">
        <img src="public/layout/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="public/layout/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav class="mt-2">

                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    <li class="nav-item">
                        <a href="home.php" class="nav-link">
                            <i class="nav-icon fas fa-chart-bar"></i>
                            <p>
                                Thống kê doanh thu
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">

                        <a href="danh_muc_tin_tuc.php" class="nav-link" >
                            <i class="nav-icon fas fa-newspaper"></i>
                            <p>
                                Quản lý tin tức
                            </p>
                            <i class="right fas fa-angle-left"></i>
                        </a>

                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="danh_muc_tin_tuc.php" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Danh mục tin tức</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tin tức</p>
                                </a>
                            </li>

                        </ul>

                    </li>
                    <li class="nav-item menu-open">
                        <a  class="nav-link active" >
                            <i class=" fas fa-graduation-cap nav-icon"></i>
                            <p>
                                Quản lý khóa học
                            </p>
                            <i class="right fas fa-angle-left"></i>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="danh_muc_khoa_hoc.php" class="nav-link">
                                    <i class=" nav-icon far fa-circle "></i>
                                    <p>Danh mục khóa học</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link active">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Khóa học</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Danh sách lớp học</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="giang_vien.php" class="nav-link">
                            <i class="nav-icon fa fa-chalkboard-teacher"></i>
                            <p>
                                Giảng viên
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="dang_ky.php" class="nav-link">
                            <i class="nav-icon fa fa-users"></i>
                            <p>
                                Đăng ký
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="khuyen_mai.php" class="nav-link">
                            <i class="nav-icon fa fa-comment-dollar"></i>
                            <p>
                                Khuyến mại
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="lien_he.php" class="nav-link">
                            <i class="nav-icon fas fa-phone"></i>
                            <p>
                                Liên hệ
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="tieu_de.php" class="nav-link">
                            <i class="nav-icon fas fa-arrow-alt-circle-up"></i>
                            <p>
                                Tiêu đề
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="logout.php" class="nav-link">
                            <i class="nav-icon fa fa-arrow-alt-circle-right"></i>
                            <p>
                                Đăng xuất
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
</aside>