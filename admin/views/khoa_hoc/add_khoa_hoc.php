<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Thêm Khóa Học</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form enctype="multipart/form-data" method="post" >
            <div class="card-body">
                <div class="form-group" >
                    <label for="exampleInputProvince">Tên khóa học</label>
                    <input type="text" class="form-control" name="ten_khoa_hoc" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Thời gian</label>
                    <input type="number" min="6" class="form-control" name="thoi_gian" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Thông tin khóa học</label>
                    <textarea name="thong_tin" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Kế Hoạch Đào Tạo</label>
                    <textarea name="ke_hoach_dao_tao" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Chọn Ảnh</label>
                    <div class="col-md-12">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="f_hinh_anh" name="f_hinh_anh" required>
                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Học Phí Khóa Học</label>
                    <input type="number" min="1000000" class="form-control" name="hoc_phi" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                        <option value="0">Không Hoạt Đông</option>
                        <option value="1">Hoạt Đông</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Danh Mục Khóa Học</label>
                    <select class="form-control" name="id_danh_muc">
                        <?php
                        foreach ($show_all as $dmkh) {
                            ?>
                            <option value="<?php echo  $dmkh->id;?>"><?php echo $dmkh->ten_danh_muc ;?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>