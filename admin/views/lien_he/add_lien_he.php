<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Thêm Liên Hệ</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="add_lien_he.php">
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputProvince">Nôi Dung Liên Hệ</label>
                    <input type="text" class="form-control" name="noi_dung" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Email</label>
                    <input type="email" class="form-control" name="email" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Số Diện Thoại</label>
                    <input type="text" max="10" class="form-control" name="phone" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Địa Chỉ</label>
                    <input type="text" class="form-control" name="dia_chi" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                        <option value="0">Không Hoạt Đông</option>
                        <option value="1">Hoạt Đông</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>