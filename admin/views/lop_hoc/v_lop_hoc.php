<!-- Main Sidebar Container -->
<?php
include ("nav.php");
include ("aside.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <?php
            if(isset($_GET['id_kh'])){

                ?>
                <div class="col-12">
                    <div class="callout callout-info">
                        <h5>Danh mục:
                            <?php
                            foreach ( $show_tenkh as $kh) {
                                echo $kh->ten_khoa_hoc;
                            }
                            ?>
                        </h5>
                    </div>
                </div>
                <?php
            }else{
                ?>
                <div class="col-12 d-flex no-block align-items-center">
                    <h1 class="page-title">
                    </h1>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title m-b-0"><a href="add_lop_hoc.php"><button type="button" class="btn btn-success btn-sm">Add New</button></a></h5>
                            <div class="card-tools">
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Số thứ tự</th>
                                <th scope="col">Tên lớp học</th>
                                <th scope="col">Ca học</th>
                                <th scope="col">Thời gian</th>
                                <th scope="col">Địa điểm</th>
                                <th scope="col">Số chỗ</th>
                                <th scope="col">Giảng viên</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Hoạt động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $show_class as $lp){

                            ?>
                            <tr>
                                <th scope="row"><?php echo $i++ ;?></th>
                                <td><a href="dang_ky.php?id_dk=<?php echo $lp->id;?>"><?php echo $lp->ten_lop_hoc ;?></a></td>
                                <td><?php echo $lp->ca_hoc ;?></td>
                                <td><?php echo date("d/m/Y",strtotime($lp->thoi_gian_bat_dau)) ?></td>
                                <td><?php echo $lp->dia_diem_hoc ;?></td>
                                <td><?php echo $lp->so_cho ;?></td>
                                <td><?php echo $lp->ten_giang_vien ;?></></td>
                                <td>

                                    <?php
                                    $date = date('Y-m-d');
                                    if($lp->trang_thai==1 && strtotime($lp->thoi_gian_bat_dau) >= strtotime ( $date )){?>
                                        <span class="badge badge-pill badge-info float">Hoạt Động</span>
                                    <?php } else {?>
                                        <span class="badge badge-pill badge-warning float">Không Hoạt Động</span>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning btn-lg" onclick="window.location.href='edit_lop_hoc.php?id=<?php echo $lp->id; ?>'" style="margin-right: 5px;"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger btn-lg" onclick="window.location.href='delete_lop_hoc.php?id=<?php echo $lp->id; ?>'" style="margin-right: 5px;"><i class="nav-icon fa fa-trash-alt"></i></button>
                                </td>
                                <td>
                            </tr>
                            </tbody>
                            <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
