<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Sửa Thông Tin Lớp Học</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" >
            <div class="card-body">
                <div class="form-group" >
                    <label for="exampleInputProvince">Tên lớp học</label>
                    <input type="text" class="form-control" name="ten_lop_hoc" value="<?php echo $lp->ten_lop_hoc;?>" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Ca hoc</label>
                    <input type="text"  class="form-control" name="ca_hoc" value="<?php echo $lp->ca_hoc;?>" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Thời gian bắt đầu</label>
                    <input type="text"  class="form-control" name="thoi_gian" value="<?php echo date('Y-m-d', strtotime($lp->thoi_gian_bat_dau));?>" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Địa Chỉ</label>
                    <input type="text"  class="form-control" name="dia_chi" value="<?php echo $lp->dia_diem_hoc;?>" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Số chỗ</label>
                    <input type="number" min="10" class="form-control" value="<?php echo $lp->so_cho;?>" name="hoc_phi" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                            <option value="1" <?php if($lp->trang_thai ==1) echo"selected";?>>Hoạt Đông</option>
                            <option value="0" <?php  if($lp->trang_thai ==0) echo"selected";?>>Không Hoạt Đông</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Khóa Học</label>
                    <select class="form-control" name="id_khoa_hoc">
                        <?php
                        foreach ($show_kh as $kh) {
                            ?>
                            <option value="<?php echo  $kh->id;?>"><?php echo $kh->ten_khoa_hoc ;?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Giảng viên</label>
                    <select class="form-control" name="id_giang_vien">
                        <?php
                        foreach ($show_gv as $gv) {
                            ?>
                            <option value="<?php echo  $gv->id;?>"><?php echo $gv->ten_giang_vien ;?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>

        </form>
    </div>
</div>
