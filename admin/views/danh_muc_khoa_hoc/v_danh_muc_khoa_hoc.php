<!-- Main Sidebar Container -->
<?php
include ("nav.php");
include ("aside.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">
                </h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title m-b-0"><a href="add_danh_muc_khoa_hoc.php"><button type="button" class="btn btn-success btn-sm">Thêm danh mục</button></a></h3>
                            <div class="card-tools">
                                <form method="post" class="form-inline ml-3">
                                <div class="input-group input-group-sm" style="width: 250px;">
                                    <input type="text" name="table_search"  class="form-control float-right" placeholder="Search">
                                    <div class="input-group-append">
                                        <input type="submit" name="submit"  class="btn btn-default">
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Số thứ tự</th>
                                <th scope="col">Tên danh mục</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Hoạt động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $show_dmkh as $dmkh){

                                ?>
                                <tr>
                                    <th><?php echo $i++ ;?></th>
                                    <td><a href="khoa_hoc.php?id_dmkh=<?php echo $dmkh->id?>"><?php echo $dmkh->ten_danh_muc ;?></a></td>
                                    <td>
                                        <?php if($dmkh->trang_thai==1){?>
                                        <span class="badge badge-pill badge-info float">Hoạt Động</span>
                                        <?php } else if ($dmkh->trang_thai==0){?>
                                            <span class="badge badge-pill badge-warning float">Không Hoạt Động</span>
                                            <?php
                                                }
                                                ?>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-warning" onclick="window.location.href='edit_danh_muc_khoa_hoc.php?id=<?php echo $dmkh->id; ?>'"><i class="fa fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger" onclick="window.location.href='delete_danh_muc_khoa_hoc.php?id=<?php echo $dmkh->id; ?>'"><i class="nav-icon fa fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
