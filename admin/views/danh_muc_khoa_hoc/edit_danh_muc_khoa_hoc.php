<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Sửa Thông Tin Danh Mục Khóa Học</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="">
            <?php
            foreach ( $show_id as $dmkh){
            ?>
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputProvince">Tên Danh Mục</label>
                    <input type="text" class="form-control" name="ten_danh_muc" value="<?php echo $dmkh->ten_danh_muc;?>" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                        <option value="1" <?php if($dmkh->trang_thai ==1) echo"selected";?>>Hoạt Đông</option>
                        <option value="0" <?php  if($dmkh->trang_thai ==0) echo"selected";?>>Không Hoạt Đông</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-warning" name="btnSave">Submit</button>
                </div>
            </div>
                <?php
            }
            ?>
        </form>
    </div>
</div>