<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1></h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Main content -->
                    <form class="form-horizontal" id="" method="post"  action="">
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->

                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <small class="float-right">Date: <?php echo  $ndk = date("d/m/Y");?></small>
                                </h4>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col" >
                                <address>
                                    <strong>Họ tên khách hàng: </strong>
                                    <?php echo $lp->ho_ten;?>
                                    <br>
                                    <strong>Số điện thoại: </strong>
                                    <?php echo $lp->so_dien_thoai;?>
                                    <br>
                                    <strong>Gmail: </strong>
                                    <?php echo $lp->email;?>
                                </address>
                            </div>
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Khóa học đăng ký</th>
                                        <th>Lớp học đăng ký</th>
                                        <th>Giảng viên</th>
                                        <th>Ca học</th>
                                        <th>Địa Điểm</th>
                                        <th>Giá tiền</th>
                                        <th>Ngày đăng ký</th>
                                        <th>Trạng thái</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><?php echo $lp->ten_khoa_hoc;?></td>
                                        <td><?php echo $lp->ten_lop_hoc;?></td>
                                        <td><?php echo $lp->ten_giang_vien;?></td>
                                        <td><?php echo $lp->ca_hoc;?></td>
                                        <td><?php echo $lp->dia_diem_hoc;?></td>
                                        <td><?php echo $lp->gia_tien;?></td>
                                        <td><?php echo $lp->ngay_dang_ky;?></td>
                                        <td>
                                        <?php if($lp->trang_thai==1){?>
                                            <span class="badge badge-pill badge-info float">Đã thanh toán</span>
                                        <?php } else if ($lp->trang_thai==0){?>
                                            <span class="badge badge-pill badge-warning float">Chưa thanh toán</span>
                                            <?php
                                        }
                                        ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <!-- this row will not appear when printing -->
                        <div class="row no-print">
                            <div class="col-12">
                                <a rel="noopener" target="_blank" class="btn btn-default" onclick="window.print()"><i class="fas fa-print"></i> Print</a>
                                <?php if($lp->trang_thai==0){?>
                                <button type="submit" name="btnSave" class="btn btn-success float-right"><i class="far fa-credit-card"></i>Cập nhập trạng thái thanh toán</button>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    </form>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>