<!-- Main Sidebar Container -->
<?php
include ("nav.php");
include ("aside.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">
                </h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Số thứ tự</th>
                                <th scope="col">Họ tên</th>
                                <th scope="col">Số điện thoại</th>
                                <th scope="col">Emai</th>
                                <th scope="col">Ngày đăng ký</th>
                                <th scope="col">Giá tiền</th>
                                <th scope="col">Lớp học</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Hoạt động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $show_all as $dmkh){

                                ?>
                                <tr>
                                    <th><?php echo $i++ ;?></th>
                                    <td><?php echo $dmkh->ho_ten ;?></td>
                                    <td><?php echo $dmkh->so_dien_thoai ;?></td>
                                    <td><?php echo $dmkh->email;?></td>
                                    <td><?php echo date("d/m/Y",strtotime($dmkh->ngay_dang_ky))  ;?></td>
                                    <td><?php echo number_format($dmkh->gia_tien) ?> VND</td>
                                    <td><?php echo $dmkh->ten_lop_hoc;?></td>
                                    <td>
                                        <?php if($dmkh->trang_thai==1){?>
                                            <span class="badge badge-pill badge-info float">Đã thanh toán</span>
                                        <?php } else if ($dmkh->trang_thai==0){?>
                                            <span class="badge badge-pill badge-warning float">Chưa thanh toán</span>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-warning" onclick="window.location.href='edit_dang_ky.php?id=<?php echo $dmkh->id; ?>'"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
