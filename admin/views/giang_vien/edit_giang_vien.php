<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Sửa Thông Tin Giảng Viên</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form enctype="multipart/form-data" method="post" >
                <div class="card-body">
                    <div class="form-group" >
                        <label for="exampleInputProvince">Tên Giảng Viên</label>
                        <input type="text" class="form-control" name="ten_giang_vien" value="<?php echo $kh->ten_giang_vien;?>"  autofocus="true" autocomplete="on" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputProvince">Thông Tin Cá Nhân</label>
                        <textarea name="noi_dung" class="form-control" required><?php echo $kh->thong_tin_ca_nhan;?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputProvince">Chọn Ảnh</label>
                        <div class="col-md-12">
                            <div class="custom-file" style="height: 120px;">
                                <input type="file" class="custom-file-input" id="f_hinh_anh" name="f_hinh_anh">
                                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                <img src="../public/layout/imggiangvien/<?php echo $kh->hinh_anh;?>" width="120px" height="100px" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputStatus">Trạng Thái</label>
                        <select class="form-control" name="trang_thai">
                            <option value="1" <?php if($kh->trang_thai ==1) echo"selected";?>>Hoạt Động</option>
                            <option value="0" <?php  if($kh->trang_thai ==0) echo"selected";?>>Không Hoạt Động</option>
                        </select>
                    </div>
                    <div>
                        <button class="btn btn-primary" name="btnSave">Submit</button>
                    </div>
                </div>
        </form>
    </div>
</div>