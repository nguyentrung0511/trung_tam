<!-- Main Sidebar Container -->
<?php
include ("nav.php");
include ("aside.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title m-b-0"><a href="add_giang_vien.php"><button type="button" class="btn btn-success btn-sm">Thêm giảng viên</button></a></h5>
                            <div class="card-tools">
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Số thứ tự</th>
                                <th scope="col">Tên giảng viên</th>
                                <th scope="col">Hình ảnh</th>
                                <th scope="col">Thông tin cá nhân</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Hoạt động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $show_all_gv as $gv){

                            ?>
                            <tr>
                                <th><?php echo $i++ ;?></a></th>
                                <td ><?php echo $gv->ten_giang_vien ;?></td>
                                <td><img src="../public/layout/imggiangvien/<?php echo  $gv->hinh_anh?>" style="width: 80px;"></td>
                                <td style="width: 320px;"><?php echo $gv->thong_tin_ca_nhan ;?></td>
                                <td>
                                    <?php if($gv->trang_thai==1){?>
                                        <span class="badge badge-pill badge-info float">Đang giảng dạy</span>
                                    <?php } else if ($gv->trang_thai==0){?>
                                        <span class="badge badge-pill badge-warning float">Ngưng giảng dạy</span>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td><button type="button" class="btn btn-warning" onclick="window.location.href='edit_giang_vien.php?id=<?php echo $gv->id; ?>'"><i class="fa fa-edit"></i></button>
                                </td>
                                <td>
                            </tr>
                            </tbody>
                            <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
