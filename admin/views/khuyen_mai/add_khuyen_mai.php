<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Thêm Khuyến Mãi</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="add_khuyen_mai.php">
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputProvince">Mã Khuyến Mãi</label>
                    <input type="text" class="form-control" name="ma_khuyen_mai" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Tên Khuyến Mãi</label>
                    <input type="text" class="form-control" name="ten_khuyen_mai" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Phần Trăm Giảm Giá</label>
                    <input type="number" min="5" class="form-control" name="phan_tram_giam_gia" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Thời Gian Bắt Đầu</label>
                    <input type="date"  class="form-control" name="thoi_gian_bat_dau" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Thời Gian Kết Thúc</label>
                    <input type="date"  class="form-control" name="thoi_gian_ket_thuc" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                        <option value="0">Không Hoạt Đông</option>
                        <option value="1">Hoạt Đông</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>