<?php
include("models/m_read_all.php");
include ("models/m_danh_muc_tin_tuc.php");
include ("models/m_read_id.php");
class c_danh_muc_tin_tuc{

    public function index(){
        $show_all = new m_read_all();
        $show_dmtt = $show_all->read_show_all_danh_muc_tin_tuc();
        $view = 'views/danh_muc_tin_tuc/v_danh_muc_tin_tuc.php';
        include("templates/layout.php");
    }
    public function add_danhmuctintuc(){
        $show_all = new m_read_all();
        $show_dmtt = $show_all->read_show_all_danh_muc_tin_tuc();
        if (isset($_POST["btnSave"])) {
            $id = null;
            $ten_dmtt = $_POST["ten_danh_muc"];
            $trang_thai = $_POST["trang_thai"];


            foreach ( $show_dmtt as $tt) {

                if ($ten_dmtt == $tt->ten_danh_muc) {
                    echo "<script>alert('Tên danh mục bị trùng thêm không thành công');window.location='danh_muc_tin_tuc.php'</script>";
                    return;
                }
            }
            $m_danh_muc = new m_danh_muc_tin_tuc();
            $kq = $m_danh_muc->add_danh_muc_tin_tuc($id, $ten_dmtt, $trang_thai);
            if ($kq) {

                echo "<script>alert('Thêm không thành công');window.location='danh_muc_tin_tuc.php'</script>";

            }
        }
        $view = 'views/danh_muc_tin_tuc/add_danh_muc_tin_tuc.php';
        include("templates/layout.php");
    }
    public function edit_danhmuctintuc(){
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            $show = new m_read_id();
            $show_id = $show->read_id_danh_muc_tin_tuc($id);
            if (isset($_POST["btnSave"])) {
                $ten_dmtt= $_POST["ten_danh_muc"];
                $trang_thai = $_POST["trang_thai"];
                $m_danh_muc = new m_danh_muc_tin_tuc();
                $kt = $m_danh_muc->edit_danh_muc_tin_tuc($ten_dmtt,$trang_thai,$id);
                if ($kt) {
                    echo "<script>alert('Sửa thông tin thành công');window.location='danh_muc_tin_tuc.php'</script>";
                }
            }
        }
        $view = 'views/danh_muc_tin_tuc/edit_danh_muc_tin_tuc.php';
        include("templates/layout.php");
    }
    public function delete_danhmuctintuc(){
        if (isset($_GET["id"])) {
            $id=$_GET["id"];
            $show_all = new m_read_all();
            $count_news=$show_all->read_all_tintuc($id);
            if(count($count_news)>0){
                echo "<script>alert('Xóa không thành công ! Trong danh mục này đã tồn tại tin tức');window.location='danh_muc_tin_tuc.php'</script>";
            }else{
                $delete = new m_danh_muc_tin_tuc();
                $kq = $delete->delete_danh_muc_tin_tuc($id);
                echo "<script>alert('Xóa thành công');window.location='danh_muc_tin_tuc.php'</script>";
            }
        }
    }
}
