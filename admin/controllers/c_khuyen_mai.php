<?php
include("models/m_read_all.php");
include ("models/m_khuyen_mai.php");
include ("models/m_read_id.php");
class c_khuyen_mai{
    public function index(){
        $show = new m_read_all();
        $show_dmkh = $show->read_all_khuyen_mai();
        $view = 'views/khuyen_mai/v_khuyen_mai.php';
        include("templates/layout.php");
    }
    public function add_khuyen_mai(){
        $show = new m_read_all();
        $show_dmkh = $show->read_all_khuyen_mai();
        if (isset($_POST["btnSave"])) {
            $id = null;
            $ma_khuyen_mai = $_POST["ma_khuyen_mai"];
            $ten_khuyen_mai = $_POST["ten_khuyen_mai"];
            $phan_tram_giam_gia = $_POST["phan_tram_giam_gia"];
            $thoi_gian_bd = date_create($_POST["thoi_gian_bat_dau"]);
            $ngay_bat_dau = date_format($thoi_gian_bd, "Y-m-d");
            $thoi_gian_kt = date_create($_POST["thoi_gian_ket_thuc"]);
            $ngay_ket_thuc= date_format($thoi_gian_kt, "Y-m-d");
            $trang_thai= $_POST["trang_thai"];
            foreach ($show_dmkh as $dm) {

                if ($ma_khuyen_mai == $dm->ma_khuyen_ma) {
                    echo "<script>alert('Mã khuyến mãi bị trùng thêm không thành công');window.location='add_khuyen_mai.php'</script>";
                    return;
                }
            }
            $m_khuyen_mai =new m_khuyen_mai();
            $kq = $m_khuyen_mai->add_khuyenmai($id, $ma_khuyen_mai,$ten_khuyen_mai,$phan_tram_giam_gia,$ngay_bat_dau,$ngay_ket_thuc,$trang_thai);
            if ($kq) {

                echo "<script>alert('Thêm không thành công');window.location='khuyen_mai.php'</script>";

            }
        }
        $view = 'views/khuyen_mai/add_khuyen_mai.php';
        include("templates/layout.php");
    }
    public function edit_khuyen_mai(){
        if (isset($_GET["id"])) {
            $id= $_GET["id"];
            $m_id =new m_read_id();
            $lp= $m_id->read_id_khuyen_mai($id);
            if (isset($_POST["btnSave"])) {
                $ma_khuyen_mai = $_POST["ma_khuyen_mai"];
                $ten_khuyen_mai = $_POST["ten_khuyen_mai"];
                $phan_tram_giam_gia = $_POST["phan_tram_giam_gia"];
                $ngay_bd = date_create($_POST["thoi_gian_bat_dau"]);
                $ngay_bat_dau = date_format($ngay_bd, 'Y-m-d');
                $ngay_kt=date_create($_POST["thoi_gian_ket_thuc"]);
                $ngay_ket_thuc= date_format($ngay_kt, 'Y-m-d');
                $trang_thai= $_POST["trang_thai"];
                $m_khuyen_mai =new m_khuyen_mai();
                $kq = $m_khuyen_mai->edit_khuyen_mai($ma_khuyen_mai,$ten_khuyen_mai,$phan_tram_giam_gia,$ngay_bat_dau,$ngay_ket_thuc,$trang_thai, $id);
                if ($kq) {
                    echo "<script>alert('Sửa thông tin thành công');window.location='khuyen_mai.php'</script>";
                }
            }
        }
        $view = 'views/khuyen_mai/edit_khuyen_mai.php';
        include("templates/layout.php");
    }
}