<?php
include("models/m_read_all.php");
include ("models/m_lien_he.php");
include ("models/m_read_id.php");
class c_lien_he{
    public function index(){
        $show_all = new m_read_all();
        $show_dmkh = $show_all->read_all_lien_he();
        $view = "views/lien_he/v_lien_he.php";
        include('templates/layout.php');
    }
    public function add_lien_he(){
        $show_all = new m_read_all();
        $show_dmkh = $show_all->read_all_lien_he();
        if (isset($_POST["btnSave"])) {
            $id = null;
            $noi_dung= $_POST["noi_dung"];
            $email= $_POST["email"];
            $phone= $_POST["phone"];
            $dia_chi= $_POST["dia_chi"];
            $trang_thai= $_POST["trang_thai"];
            $m_lien_he= new m_lien_he();
            $kq= $m_lien_he->add_lien_he($id,$noi_dung,$email,$phone,$dia_chi,$trang_thai);
            if ($kq) {

                echo "<script>alert('Thêm không thành công');window.location='lien_he.php'</script>";

            }
        }
        $view = "views/lien_he/add_lien_he.php";
        include('templates/layout.php');
    }
    public function edit_lien_he(){
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            $show_all = new m_read_id();
            $show_dmkh = $show_all->read_id_lien_he($id);
            if (isset($_POST["btnSave"])) {
                $noi_dung= $_POST["noi_dung"];
                $email= $_POST["email"];
                $phone= $_POST["phone"];
                $dia_chi= $_POST["dia_chi"];
                $trang_thai= $_POST["trang_thai"];
                $m_lien_he= new m_lien_he();
                $kq= $m_lien_he->edit_lien_he($noi_dung,$email,$phone,$dia_chi,$trang_thai,$id);
                if ($kq) {
                    echo "<script>alert('Sửa thông tin thành công');window.location='lien_he.php'</script>";
                }
            }
        }
        $view = "views/lien_he/edit_lien_he.php";
        include('templates/layout.php');
    }
}