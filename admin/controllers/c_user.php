<?php
@session_start();
include ("models/m_user.php");
class c_user {
    public function check_login()
    {
        $flag = false;
        if (isset($_POST['login']))
        {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $m_user = new m_user();
            $this->save_login_to_session($username,$password);
            $user = $m_user->read_user_by_username($username);
            if(isset($_SESSION['user_admin']))
            {
                unset($_SESSION['error_login']);
                header("location:home.php");
            }
            else
            {
                $_SESSION['error_login'] = "Sai thông tin đăng nhập";
                header("location:login.php");

            }

        }

    }
    public function logout()
    {
        unset($_SESSION['user_admin']);
        header("location:login.php");
    }
    public function save_login_to_session($username,$password)
    {
        $m_user = new m_user();
        $user = $m_user->read_user_by_id_pass($username,$password);
        if (!empty($user))
        {
            $_SESSION['user_admin'] = $user;
        }

    }
}