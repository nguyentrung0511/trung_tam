<?php
include("models/m_read_all.php");
include ("models/m_danh_muc_tin_tuc.php");
include ("models/m_tin_tuc.php");
include ("models/m_read_id.php");
include ("SimpleImage.php");
class c_tin_tuc{
    public function index(){
        if(isset($_GET['id_dmtt'])) {
            $id = $_GET['id_dmtt'];
            $show = new m_read_id();
            $show_kh = $show->read_id_tin_tuc($id);
            $show_tendm = $show->read_id_danh_muc_tin_tuc($id);
        }
        $view = "views/tin_tuc/v_tin_tuc.php";
        include('templates/layout.php');
    }
    public function add_tin_tuc(){
        $show =new m_read_all();
        $show_dmtt= $show->read_show_all_danh_muc_tin_tuc();
        if(isset($_POST["btnSave"])) {
            $id = null;
            $ten_tin_tuc= $_POST["ten_tin_tuc"];
            $hinh_anh = ($_FILES['f_hinh_anh']['error'] == 0) ? $_FILES['f_hinh_anh']['name'] : "";
            $noi_dung = $_POST["noi_dung"];
            date_default_timezone_set("Asia/Ha_Noi");
            $ngay_tao = date("Y/m/d");
            $trang_thai = $_POST["trang_thai"];
            $id_danh_muc_tin_tuc =$_POST["id_danh_muc"];
            $ma= $_POST["id_danh_muc"];
            $show = new m_read_all();
            $show_all_khoa_hoc = $show->read_show_all_tin_tuc();
            foreach ($show_all_khoa_hoc as $kh) {
                if ($ten_tin_tuc == $kh->ten_tin_tuc) {
                    echo "<script>alert('Tên tin tức bị trùng thêm không thành công');window.location='add_tin_tuc.php'</script>";
                    return;
                }
            }
            $add = new m_tin_tuc();
            $add_khoa_hoc = $add->add_tintuc($id,$ten_tin_tuc,$hinh_anh,$noi_dung,$ngay_tao,$trang_thai,$id_danh_muc_tin_tuc);
            if ($add_khoa_hoc) {
                if ($hinh_anh != "") {
                    move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "../public/layout/imgtintuc/$hinh_anh");
                }
                echo "<script>window.location='tin_tuc.php?id_dmtt=".$ma."'</script>";
            } else {
                echo "<script>alert('thêm không thành công')</script>";
            }
        }
        $view = "views/tin_tuc/add_tin_tuc.php";
        include('templates/layout.php');
    }
    public function edit_tin_tuc(){
        if(isset($_GET["id"])){
            $id = $_GET["id"];
            $show = new m_read_id();
            $kh=$show->read_show_id_tin_tuc($id);
            $show_dmkh= new m_read_all();
            $show_all= $show_dmkh->read_show_all_danh_muc_tin_tuc();
            if (isset($_POST['btnSave'])){
                $ten_tin_tuc= $_POST["ten_tin_tuc"];
                $hinh_anh = ($_FILES['f_hinh_anh']['error'] == 0) ? $_FILES['f_hinh_anh']['name'] :$kh->hinh_anh;
                $noi_dung = $_POST["noi_dung"];
                $trang_thai = $_POST["trang_thai"];
                $id_danh_muc_tin_tuc =$_POST["id_danh_muc"];
                $ma= $_POST["id_danh_muc"];
                $edit = new m_tin_tuc();
                $edit_it = $edit->edit_tintuc($id,$ten_tin_tuc,$hinh_anh,$noi_dung,$trang_thai,$id_danh_muc_tin_tuc);
                if ($edit_it) {
                    if ($_FILES["f_hinh_anh"]["error"] == 0) {
                        move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "../public/layout/imgtintuc/$hinh_anh");
                    }
                    echo "<script>alert('Cập nhật thành công');window.location='tin_tuc.php?id_dmtt=".$ma."'</script>";
                } else {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }
            }
        }
        $view = "views/tin_tuc/edit_tin_tuc.php";
        include('templates/layout.php');
    }
    public function delete_tin_tuc(){
        if(isset($_GET["id"])) {
            $id = $_GET["id"];
            $tl= new m_read_id();
            $tra_lai = $tl->read_id_tra_lai_tin_tuc($id);
            $show_ve = $tra_lai[0]->id;
                $delete = new m_tin_tuc();
                $kq = $delete->delete_tin_tuc($id);
                echo "<script>alert('Xóa không thành công');window.location='tin_tuc.php?id_dmtt=".$show_ve."'</script>";

        }
    }
}