<?php
class c_home{
    public function show_all(){
        include_once ("models/m_thong_ke.php");
        $m_thong_ke = new m_thong_ke();
        $lp = $m_thong_ke->conut_lop_hoc();
        $kh = $m_thong_ke->conut_khoa_hoc();
        $gv = $m_thong_ke->conut_giang_vien();
        $hv = $m_thong_ke->conut_hoc_vien();
        $hvn = $m_thong_ke->conut_hoc_vien_ngay();
        $km = $m_thong_ke->conut_khuyen_mai();
        $hoa_don=$m_thong_ke->thong_ke_doanh_so();
        $thang=array();
        $tong=array();

        foreach($hoa_don as $hd)
        {
            $thang[]=$hd->ThangNam;
            $tong[]=(float)$hd->tong;

        }

        $arr=array(
            "labels"=>$thang,
            "datasets"=>array(
                array(
                    "label"=>"Doanh thu theo tháng năm ",
                    "borderColor"=>"#00CCFF",
                    "backgroundColor"=>"#66FFFF",
                    "fillColor"=>"rgba(172,194,132,0.4)",
                    "strokeColor"=>"#ACC26D",
                    "pointBorderColor"=>"#0C0",
                    "pointStrokeColor"=>"#9DB86D",
                    "pointBorderWidth" => 1,
                    "pointHoverRadius"=>5,
                    "pointHoverBorderWidth"=>3,
                    "pointRadius"=>5,
                    "pointHitRadius"=>10,
                    "data"=>$tong
                )
            )
        );
        $strJSON=json_encode($arr);
        $view = "views/home/v_home.php";
        include('templates/layout.php');
    }
}