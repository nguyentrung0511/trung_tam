<?php
include("models/m_read_all.php");
include ("models/m_read_id.php");
include ("models/m_lop_hoc.php");
include ("SimpleImage.php");
class c_lop_hoc{
    public function index(){
        if(isset($_GET['id_kh']))
        {
            $id_kh = $_GET['id_kh'];
            $show = new m_read_id();
            $show_class=$show->read_id_lop_hoc($id_kh);
            $show_tenkh=$show->read_id_khoahoc($id_kh);
        }
        $view = "views/lop_hoc/v_lop_hoc.php";
        include('templates/layout.php');
    }
    public function add_lophoc(){
        $show= new m_read_all();
        $show_kh = $show->read_show_all_khoa_hoc();
        $show_gv = $show->read_show_all_giang_vien();
        if (isset($_POST["btnSave"])) {

            $id = null;
            $ten_lop_hoc = $_POST["ten_lop_hoc"];
            $ca_hoc = $_POST["ca_hoc"];
            $thoi_gian_bd = date_create($_POST["thoi_gian"]);
            $thoi_gian_bat_dau = date_format($thoi_gian_bd, "Y-m-d");
            $dia_diem = $_POST["dia_chi"];
            $so_cho = $_POST["hoc_phi"];
            $trang_thai = $_POST["trang_thai"];
            $id_khoa_hoc = $_POST["id_khoa_hoc"];
            $tra_ve = $_POST["id_khoa_hoc"];
            $id_giang_vien = $_POST["id_giang_vien"];
            $show_lp = $show->read_show_all_lop_hoc();
            foreach ($show_lp as $lp) {
                if ($ten_lop_hoc == $lp->ten_lop_hoc) {
                    echo "<script>alert('Tên lớp học bị trùng thêm không thành công');window.location='add_lop_hoc.php'</script>";
                    return;
                }
            }
                $lop_hoc = new m_lop_hoc();
                $add = $lop_hoc->add_lop_hoc($id, $ten_lop_hoc, $ca_hoc, $thoi_gian_bat_dau, $dia_diem, $so_cho, $trang_thai, $id_khoa_hoc, $id_giang_vien);
                if ($add) {
                    echo "<script>alert('Thêm không thành công');window.location='lop_hoc.php?id_kh=" . $tra_ve . "'</script>";
                } else {
                    echo "<script>alert('Bạn nhập lớp chưa thành công ');window.location='add_lop_hoc.php'</script>";
                }
        }
        $view = "views/lop_hoc/add_lop_hoc.php";
        include('templates/layout.php');
    }
    public function edit_lop_hoc(){
        if (isset($_GET["id"])) {
            $show= new m_read_all();
            $show_kh = $show->read_show_all_khoa_hoc();
            $show_gv = $show->read_show_all_giang_vien();
            $id = $_GET["id"];
            $show_id = new m_read_id();
            $lp = $show_id->read_id_lophoc($id);
            if (isset($_POST["btnSave"])) {
                $date = date('Y-m-d');
                $newdate = strtotime ( '+3 month' , strtotime ( $date ) ) ;
                $ten_lop_hoc = $_POST["ten_lop_hoc"];
                $ca_hoc = $_POST["ca_hoc"];
                $thoi_gian = date_create($_POST["thoi_gian"]);
                $thoi_gian_bat_dau=date_format($thoi_gian, "Y-m-d");
                $dia_diem = $_POST["dia_chi"];
                $so_cho = $_POST["hoc_phi"];
                $trang_thai = $_POST["trang_thai"];
                $id_khoa_hoc = $_POST["id_khoa_hoc"];
                $tra_ve = $_POST["id_khoa_hoc"];
                $id_giang_vien = $_POST["id_giang_vien"];
                $lop_hoc= new m_lop_hoc();
                $edit_lp = $lop_hoc->edit_lop_hoc($ten_lop_hoc,$ca_hoc,$thoi_gian_bat_dau,$dia_diem,$so_cho,$trang_thai,$id_khoa_hoc,$id_giang_vien,$id);
                if ($edit_lp) {
                    echo "<script>alert('Sửa thông tin thành công');window.location='lop_hoc.php?id_kh=".$tra_ve."'</script>";
                }
                }

            }
        $view = "views/lop_hoc/edit_lop_hoc.php";
        include('templates/layout.php');
    }
    public function delete_lop_hoc(){
        if (isset($_GET["id"])) {
            $id=$_GET["id"];
            $tl= new m_read_id();
            $tra_lai = $tl->read_id_tra_lai_lop($id);
            $show_ve = $tra_lai[1]->id;
            $show_all = new m_read_all();
            $count_class=$show_all->read_all_dang_ky($id);
            if(count($count_class)>0){
                echo "<script>alert('Xóa không thành công ! Trong lớp học này đã tồn tại học viên đăng ký');window.location='lop_hoc.php?id_kh=".$show_ve."'</script>";
            }else{
                $delete = new m_lop_hoc();
                $kq = $delete->delete_lop_hoc($id);
                echo "<script>alert('Xóa thành công');window.location='lop_hoc.php?id_kh=".$show_ve."'</script>";
            }
        }
    }
}