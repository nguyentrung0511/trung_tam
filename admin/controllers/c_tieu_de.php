<?php
include("models/m_read_all.php");
include ("models/m_tieu_de.php");
include ("models/m_read_id.php");
include ("SimpleImage.php");
class c_tieu_de{
    public function index(){
        $show= new m_read_all();
        $show_all=$show->read_all_tieu_de();
        $view = "views/tieu_de/v_tieu_de.php";
        include('templates/layout.php');
    }
    public function add_tieu_de(){
        $show= new m_read_all();
        $show_all=$show->read_all_tieu_de();
        if(isset($_POST["btnSave"])) {
            $id = null;
            $ten_tieu_de=$_POST["ten_tieu_de"];
            $hinh_anh = ($_FILES['f_hinh_anh']['error'] == 0) ? $_FILES['f_hinh_anh']['name'] : "";
            $trang_thai=$_POST["trang_thai"];
            foreach ($show_all as $kh) {
                if ($ten_tieu_de == $kh->ten_tieu_de ) {
                    echo "<script>alert('Tên tiêu đề bị trùng thêm không thành công');window.location='add_tieu_de.php'</script>";
                    return;
                }
            }
            $add = new m_tieu_de();
            $add_tt = $add->add_tieu_de($id,$ten_tieu_de,$hinh_anh,$trang_thai);
            if ($add_tt) {
                if ($hinh_anh != "") {
                    move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "../public/layout/imgtieude/$hinh_anh");
                }
                echo "<script>window.location='tieu_de.php'</script>";
            } else {
                echo "<script>alert('thêm không thành công')</script>";
            }
        }
        $view = "views/tieu_de/add_tieu_de.php";
        include('templates/layout.php');
    }
    public function edit_tieu_de(){
        if(isset($_GET["id"])){
            $id = $_GET["id"];
            $show = new m_read_id();
            $kh=$show->read_show_id_tieu_de($id);
            if (isset($_POST['btnSave'])){
                $ten_tieu_de=$_POST["ten_tieu_de"];
                $hinh_anh = ($_FILES['f_hinh_anh']['error'] == 0) ? $_FILES['f_hinh_anh']['name']  : $kh->hinh_anh;
                $trang_thai=$_POST["trang_thai"];
                $edit = new m_tieu_de();
                $edit_it = $edit->edit_tieu_de($id,$ten_tieu_de,$hinh_anh,$trang_thai);
                if ($edit_it) {
                    if ($_FILES["f_hinh_anh"]["error"] == 0) {
                        move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "../public/layout/imgtieude/$hinh_anh");
                    }
                    echo "<script>alert('Cập nhật thành công');window.location='tieu_de.php'</script>";
                } else {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }
            }
        }
        $view = "views/tieu_de/edit_tieu_de.php";
        include('templates/layout.php');
    }
}