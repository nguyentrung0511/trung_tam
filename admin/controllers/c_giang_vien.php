<?php
include("models/m_read_all.php");
include ("models/m_giang_vien.php");
include ("models/m_read_id.php");
include ("SimpleImage.php");
class c_giang_vien{
    public function index(){
        if(isset($_GET['id_gv'])){
            $id=$_GET['id_gv'];
            $show_idgv= new m_read_id();
            $show_all_gv= $show_idgv->read_id_giang_vien($id);
        }else{
            $show_idgv= new m_read_all();
            $show_all_gv= $show_idgv->read_show_all_giang_vien();
        }
        $view = "views/giang_vien/v_giang_vien.php";
        include('templates/layout.php');
    }
    public function add_giang_vien(){
        if(isset($_POST["btnSave"])) {
            $id = null;
            $ten_giang_vien =$_POST["ten_giang_vien"];
            $hinh_anh=($_FILES['f_hinh_anh']['error'] == 0) ? $_FILES['f_hinh_anh']['name'] : "";
            $thong_tin_ca_nhan=$_POST["noi_dung"];
            $trang_thai = $_POST["trang_thai"];
            $phan_quyen = 2;
            $add= new m_giang_vien();
            $add_show= $add->add_giang_vien($id,$ten_giang_vien,$hinh_anh,$thong_tin_ca_nhan,$trang_thai,$phan_quyen);
            if ($add_show) {
                if ($hinh_anh != "") {
                    move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "../public/layout/imggiangvien/$hinh_anh");
                }
                echo "<script>window.location='giang_vien.php'</script>";
            } else {
                echo "<script>alert('thêm không thành công')</script>";
            }
        }
        $view = "views/giang_vien/add_giang_vien.php";
        include('templates/layout.php');
    }
    public function edit_giang_vien(){
        if(isset($_GET["id"])) {
            $id = $_GET["id"];
            $show_id= new m_read_id();
            $kh= $show_id->read_id_giang_vien($id);
            if (isset($_POST['btnSave'])){
                $ten_giang_vien =$_POST["ten_giang_vien"];
                $hinh_anh=($_FILES['f_hinh_anh']['error'] == 0) ? $_FILES['f_hinh_anh']['name'] : $kh->hinh_anh ;
                $thong_tin_ca_nhan=$_POST["noi_dung"];
                $trang_thai = $_POST["trang_thai"];
                $phan_quyen = 2;
                $edit= new m_giang_vien();
                $edit_show = $edit->edit_giang_vien($id,$ten_giang_vien,$hinh_anh,$thong_tin_ca_nhan,$trang_thai,$phan_quyen);
                if ($edit_show) {
                    if ($_FILES["f_hinh_anh"]["error"] == 0) {
                        move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "../public/layout/imggiangvien/$hinh_anh");
                    }
                    echo "<script>alert('Cập nhật thành công');window.location='giang_vien.php'</script>";
                } else {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }
            }
        }
        $view = "views/giang_vien/edit_giang_vien.php";
        include('templates/layout.php');
    }
}