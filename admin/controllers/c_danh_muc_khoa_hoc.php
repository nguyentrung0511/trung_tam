<?php
include("models/m_read_all.php");
include ("models/m_danh_muc_khoa_hoc.php");
include ("models/m_khoa_hoc.php");
include ("models/m_read_id.php");
class c_danh_muc_khoa_hoc{
    /*hiển thị và tìm kiếm danh mục khóa học*/
    public function index(){
        if (isset($_POST["submit"])){
            if(isset($_POST["table_search"])&& $_POST["table_search"]){
                $ten_dmkk= $_POST["table_search"];
                $show_ten = new m_read_id();
                $show_dmkh= $show_ten->read_ten_danh_muc($ten_dmkk);
            }else{
                $show_all = new m_read_all();
                $show_dmkh = $show_all->read_all_danh_muc_khoa_hoc();
            }

        }else{
            $show_all = new m_read_all();
            $show_dmkh = $show_all->read_all_danh_muc_khoa_hoc();
        }
        $view = "views/danh_muc_khoa_hoc/v_danh_muc_khoa_hoc.php";
        include('templates/layout.php');
    }
    /*Thêm danh mục khóa học mới*/
    public function add_danhmuckhoahoc(){
        $show_all = new m_read_all();
        $show_dmkh = $show_all->read_all_danh_muc_khoa_hoc();
        if (isset($_POST["btnSave"])) {
            $id = null;
            $ten_dmkh = $_POST["ten_danh_muc"];
            $trang_thai = $_POST["trang_thai"];


            foreach ($show_dmkh as $dm) {

                if ($ten_dmkh == $dm->ten_danh_muc) {
                    echo "<script>alert('Tên danh mục bị trùng thêm không thành công');window.location='add_danh_muc_khoa_hoc.php'</script>";
                    return;
                }
            }
            $m_danh_muc = new m_danh_muc_khoa_hoc();
            $kq = $m_danh_muc->add_danh_muc_khoa_hoc($id, $ten_dmkh, $trang_thai);
            if ($kq) {

                echo "<script>alert('Thêm thành công');window.location='danh_muc_khoa_hoc.php'</script>";

            }
        }
        $view = 'views/danh_muc_khoa_hoc/add_danh_muc_khoa_hoc.php';
        include("templates/layout.php");
    }
    /*Sửa thông tin danh mục khóa học mới*/
    public function edit_danhmuckhoahoc(){
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            $show = new m_read_id();
            $show_id = $show->read_id_danh_muc_khoa_hoc($id);
            if (isset($_POST["btnSave"])) {
                $ten_dmkh= $_POST["ten_danh_muc"];
                $trang_thai = $_POST["trang_thai"];
                $m_danh_muc = new m_danh_muc_khoa_hoc();
                $kt = $m_danh_muc->edit_danh_muc_khoa_hoc($ten_dmkh,$trang_thai,$id);
                if ($kt) {
                    echo "<script>alert('Sửa thông tin thành công');window.location='danh_muc_khoa_hoc.php'</script>";
                }
            }
        }
            $view = 'views/danh_muc_khoa_hoc/edit_danh_muc_khoa_hoc.php';
            include("templates/layout.php");
    }
    public function delete_danhmuckhoahoc(){
        if (isset($_GET["id"])) {
            $id=$_GET["id"];
            $show_all = new m_read_all();
            $count_class=$show_all->read_all_khoahoc($id);
            if(count($count_class)>0){
                echo "<script>alert('Xóa không thành công ! Trong danh mục này đã tồn tại khóa học');window.location='danh_muc_khoa_hoc.php'</script>";
            }else{
                $delete = new m_danh_muc_khoa_hoc();
                $kq = $delete->delete_danh_muc_khoa_hoc($id);
                echo "<script>alert('Xóa thành công');window.location='danh_muc_khoa_hoc.php'</script>";
            }
        }
    }
}