<?php
require_once ("database.php");
class m_giang_vien extends database{
    public function add_giang_vien($id,$ten_giang_vien,$hinh_anh,$thong_tin_ca_nhan,$trang_thai,$phan_quyen){
        $sql ="insert into giang_vien values(?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$ten_giang_vien,$hinh_anh,$thong_tin_ca_nhan,$trang_thai,$phan_quyen));
    }
    public function edit_giang_vien($id,$ten_giang_vien,$hinh_anh,$thong_tin_ca_nhan,$trang_thai,$phan_quyen)
    {
        $sql="update giang_vien set ten_giang_vien=?,hinh_anh=?,thong_tin_ca_nhan=?,trang_thai=?,phan_quyen=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten_giang_vien,$hinh_anh,$thong_tin_ca_nhan,$trang_thai,$phan_quyen,$id));
    }
}