<?php
require_once ("database.php");
class m_dang_ky extends database
{
    public function edit_dangky($id,$trang_thai)
    {
        $sql = "update dang_ky set trang_thai=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($trang_thai, $id));
    }
}