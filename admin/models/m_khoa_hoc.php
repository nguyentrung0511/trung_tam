<?php
require_once ("database.php");
class m_khoa_hoc extends database{
    public function add_khoa_hoc($id,$ten_khoa_hoc,$thoi_gian,$thong_tin,$ke_hoach,$hinh_anh,$hoc_phi,$trang_thai,$id_danh_muc){
        $sql ="insert into khoa_hoc values(?,?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$ten_khoa_hoc,$thoi_gian,$thong_tin,$ke_hoach,$hinh_anh,$hoc_phi,$trang_thai,$id_danh_muc));
    }
    public function edit_khoa_hoc($id,$ten_khoa_hoc,$thoi_gian,$thong_tin,$ke_hoach,$hinh_anh,$hoc_phi,$trang_thai,$id_danh_muc)
    {
        $sql="update khoa_hoc set ten_khoa_hoc=?,thoi_gian=?,thong_tin=?,ke_hoach_dao_tao=?,hinh_anh=?,hoc_phi=?,trang_thai=?,id_danh_muc_khoa_hoc=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten_khoa_hoc,$thoi_gian,$thong_tin,$ke_hoach,$hinh_anh,$hoc_phi,$trang_thai,$id_danh_muc,$id));
    }
    public function delete_khoa_hoc($id){

        $sql = "delete from khoa_hoc where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}