<?php
require_once ("database.php");
class m_danh_muc_khoa_hoc extends database{
    public function add_danh_muc_khoa_hoc($id,$ten_dmkh,$trang_thai){
        $sql ="insert into danh_muc_khoa_hoc values(?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$ten_dmkh,$trang_thai));
    }
    public function edit_danh_muc_khoa_hoc($ten_dmkh,$trang_thai,$id)
    {
        $sql="update danh_muc_khoa_hoc set ten_danh_muc=?,trang_thai=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten_dmkh,$trang_thai,$id));
    }
    public function delete_danh_muc_khoa_hoc($id){

        $sql = "delete from danh_muc_khoa_hoc where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}