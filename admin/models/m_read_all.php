<?php
require_once ("database.php");
class m_read_all extends database{
    public function read_all_danh_muc_khoa_hoc()
    {
        $sql = "select * from danh_muc_khoa_hoc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_khoahoc($id)
    {
        $sql = "select * from khoa_hoc where id_danh_muc_khoa_hoc=".$id;
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_tintuc($id)
    {
        $sql = "select * from tin_tuc where id_danh_muc_tin_tuc=".$id;
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_khoa_hoc()
    {
        $sql = "SELECT kh.id, kh.ten_khoa_hoc,kh.thoi_gian,kh.hoc_phi,dm.ten_danh_muc,kh.trang_thai 
                FROM khoa_hoc as kh , danh_muc_khoa_hoc as dm WHERE kh.id_danh_muc_khoa_hoc = dm.id ORDER BY dm.ten_danh_muc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_detail_khoa_hoc($id)
    {
        $sql = "SELECT kh.id, kh.ten_khoa_hoc,kh.thong_tin,kh.ke_hoach_dao_tao,kh.hinh_anh,kh.thoi_gian,kh.hoc_phi,dm.ten_danh_muc,kh.trang_thai 
                FROM khoa_hoc as kh , danh_muc_khoa_hoc as dm WHERE kh.id_danh_muc_khoa_hoc = dm.id and kh.id=".$id;
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_show_all_khoa_hoc()
    {
        $sql = "select * from khoa_hoc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_show_all_tin_tuc()
    {
        $sql = "select * from tin_tuc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_lop($id)
    {
        $sql = "select * from lop_hoc where id_khoa_hoc=".$id;
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_dang_ky($id)
    {
        $sql = "select * from dang_ky where id_lop=".$id;
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_show_all_giang_vien()
    {
        $sql = "select * from giang_vien";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_show_all_lop_hoc()
    {
        $sql = "select * from lop_hoc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_show_all_danh_muc_tin_tuc()
    {
        $sql = "select * from danh_muc_tin_tuc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_tieu_de()
    {
        $sql = "select * from tieu_de";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_lien_he()
    {
        $sql = "select * from lien_he";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_khuyen_mai()
    {
        $sql = "select * from khuyen_mai";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_show_dang_ky()
    {
        $sql = "select dk.id,dk.ho_ten,dk.so_dien_thoai,dk.email,dk.ngay_dang_ky,dk.gia_tien,dk.trang_thai,lp.ten_lop_hoc,dk.id_lop
            from dang_ky as dk ,lop_hoc as lp where lp.id = dk.id_lop";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
}