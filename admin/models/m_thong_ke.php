<?php
require_once ("database.php");
class m_thong_ke extends database{
    public function conut_lop_hoc(){
        $sql = "select count(*) as LP from lop_hoc";
        $this->setQuery($sql);
        return $this->loadRow();
    }
    public function conut_khoa_hoc(){
        $sql = "select count(*) as KH from khoa_hoc";
        $this->setQuery($sql);
        return $this->loadRow();
    }
    public function conut_giang_vien(){
        $sql = "select count(*) as GV from giang_vien";
        $this->setQuery($sql);
        return $this->loadRow();
    }
    public function conut_hoc_vien(){
        $sql = "select count(*) as HV from dang_ky where trang_thai = 1";
        $this->setQuery($sql);
        return $this->loadRow();
    }
    public function conut_khuyen_mai(){
        $sql = "select count(*) as KM from khuyen_mai where trang_thai = 1";
        $this->setQuery($sql);
        return $this->loadRow();
    }
    public function conut_hoc_vien_ngay(){
        $sql = "select count(distinct id) as HVN from dang_ky where ngay_dang_ky = date(now())";
        $this->setQuery($sql);
        return $this->loadRow();
    }
    public function thong_ke_doanh_so(){
        $sql='SELECT concat(Month(dkh.ngay_dang_ky),"-",Year(dkh.ngay_dang_ky)) as ThangNam, sum(dkh.gia_tien) as tong 
            FROM dang_ky as dkh where dkh.trang_thai in(0,1) group by Month(dkh.ngay_dang_ky),Year(dkh.ngay_dang_ky) 
            order by Month(dkh.ngay_dang_ky), Year(dkh.ngay_dang_ky)';
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
}