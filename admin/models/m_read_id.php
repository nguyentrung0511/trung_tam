<?php
require_once ("database.php");
class m_read_id extends database{
    public function read_id_danh_muc_khoa_hoc($id){
        $sql = "select * from danh_muc_khoa_hoc where id=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_id_danh_muc_tin_tuc($id){
        $sql = "select * from danh_muc_tin_tuc where id=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_ten_danh_muc($ten_dmkk){
        $sql = "select * from danh_muc_khoa_hoc where ten_danh_muc like '%$ten_dmkk%'";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_id_khoa_hoc($id)
    {
        $sql = "select * from khoa_hoc where id_danh_muc_khoa_hoc = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_id_tin_tuc($id)
    {
        $sql = "select * from tin_tuc where id_danh_muc_tin_tuc = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_show_id_khoa_hoc($id)
    {
        $sql = "select * from khoa_hoc where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_show_id_tin_tuc($id)
    {
        $sql = "select * from tin_tuc where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_show_id_tieu_de($id)
    {
        $sql = "select * from tieu_de where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_id_tra_lai($id)
    {
        $sql = "SELECT dm.id FROM khoa_hoc AS kh ,danh_muc_khoa_hoc AS dm WHERE dm.id = kh.id_danh_muc_khoa_hoc AND kh.id = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_id_tra_lai_tin_tuc($id)
    {
        $sql = "SELECT dm.id FROM tin_tuc AS tt ,danh_muc_tin_tuc AS dm WHERE dm.id = tt.id_danh_muc_tin_tuc AND tt.id = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_id_tra_lai_lop($id)
    {
        $sql = "SELECT kh.id FROM khoa_hoc AS kh ,lop_hoc AS lp WHERE kh.id = lp.id_khoa_hoc AND lp.id = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_id_tra_lai_so_cho($id)
    {
        $sql = "SELECT lp.so_cho FROM lop_hoc AS lp ,dang_ky AS dk WHERE lp.id = dk.id_lop AND lp.id= ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_id_lop_hoc($id)
    {
        $sql = "SELECT lp.id,lp.ten_lop_hoc,lp.ca_hoc,lp.thoi_gian_bat_dau,lp.dia_diem_hoc,lp.so_cho,lp.trang_thai,gv.ten_giang_vien,kh.ten_khoa_hoc,lp.id_giang_vien FROM lop_hoc AS lp , giang_vien AS gv ,khoa_hoc as kh 
                WHERE lp.id_giang_vien = gv.id and lp.id_khoa_hoc=kh.id and lp.id_khoa_hoc = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_id_khoahoc($id){
        $sql = "select * from khoa_hoc where id=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_id_lophoc($id){
        $sql = "select * from lop_hoc where id=?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_id_giang_vien($id){
        $sql = "select * from giang_vien where id=?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_id_lien_he($id){
        $sql = "select * from lien_he where id=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_id_khuyen_mai($id){
        $sql = "select * from khuyen_mai where id=?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_id_dang_ky($id){
        $sql = "SELECT dk.id,dk.ho_ten,dk.so_dien_thoai,dk.email,dk.ngay_dang_ky,dk.gia_tien,dk.id_lop,dk.trang_thai,gv.ten_giang_vien ,kh.ten_khoa_hoc,lp.ten_lop_hoc,lp.ca_hoc,lp.dia_diem_hoc
                FROM lop_hoc as lp , giang_vien as gv, dang_ky AS dk, khoa_hoc AS kh 
                WHERE dk.id_lop = lp.id and lp.id_giang_vien= gv.id and lp.id_khoa_hoc = kh.id AND dk.id =?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
}