<?php
require_once ("database.php");
class m_danh_muc_tin_tuc extends database{
    public function add_danh_muc_tin_tuc($id, $ten_dmtt, $trang_thai){
        $sql ="insert into danh_muc_tin_tuc values(?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id, $ten_dmtt, $trang_thai));
    }
    public function edit_danh_muc_tin_tuc($ten_dmtt,$trang_thai,$id)
    {
        $sql="update danh_muc_tin_tuc set ten_danh_muc=?,trang_thai=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten_dmtt,$trang_thai,$id));
    }
    public function delete_danh_muc_tin_tuc($id){

        $sql = "delete from danh_muc_tin_tuc where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}