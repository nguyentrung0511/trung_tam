<?php
require_once ("database.php");
class m_khuyen_mai extends database
{
    public function add_khuyenmai($id, $ma_khuyen_mai,$ten_khuyen_mai,$phan_tram_giam_gia,$ngay_bat_dau,$ngay_ket_thuc,$trang_thai)
    {
        $sql = "insert into khuyen_mai values(?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id, $ma_khuyen_mai,$ten_khuyen_mai,$phan_tram_giam_gia,$ngay_bat_dau,$ngay_ket_thuc,$trang_thai));
    }

    public function edit_khuyen_mai($ma_khuyen_mai,$ten_khuyen_mai,$phan_tram_giam_gia,$ngay_bat_dau,$ngay_ket_thuc,$trang_thai, $id)
    {
        $sql = "update khuyen_mai set ma_khuyen_mai=?,ten_khuyen_mai=?,phan_tram_giam_gia=?,ngay_bat_dau=?,ngay_ket_thuc=?,trang_thai=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ma_khuyen_mai,$ten_khuyen_mai,$phan_tram_giam_gia,$ngay_bat_dau,$ngay_ket_thuc,$trang_thai, $id));
    }
}