<?php
require_once ("database.php");
class m_lop_hoc extends database {

 public function add_lop_hoc($id,$ten_lop_hoc,$ca_hoc,$thoi_gian_bat_dau,$dia_diem,$so_cho,$trang_thai,$id_khoa_hoc,$id_giang_vien){
     $sql ="insert into lop_hoc values(?,?,?,?,?,?,?,?,?)";
     $this->setQuery($sql);
     return $this->execute(array($id,$ten_lop_hoc,$ca_hoc,$thoi_gian_bat_dau,$dia_diem,$so_cho,$trang_thai,$id_khoa_hoc,$id_giang_vien));
 }
    public function edit_lop_hoc($ten_lop_hoc,$ca_hoc,$thoi_gian_bat_dau,$dia_diem,$so_cho,$trang_thai,$id_khoa_hoc,$id_giang_vien,$id){
        $sql ="update lop_hoc set ten_lop_hoc=?, ca_hoc=?,thoi_gian_bat_dau=?,dia_diem_hoc=?,so_cho=?,trang_thai=?,id_khoa_hoc=?,id_giang_vien=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten_lop_hoc,$ca_hoc,$thoi_gian_bat_dau,$dia_diem,$so_cho,$trang_thai,$id_khoa_hoc,$id_giang_vien,$id));
    }
    public function edit_so_cho($id,$so_cho){
        $sql ="update lop_hoc set so_cho=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($so_cho,$id));
    }
    public function delete_lop_hoc($id){
        $sql = "delete from lop_hoc where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}