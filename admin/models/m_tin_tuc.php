<?php
require_once ("database.php");
class m_tin_tuc extends database{
    public function add_tintuc($id,$ten_tin_tuc,$hinh_anh,$noi_dung,$ngay_tao,$trang_thai,$id_danh_muc_tin_tuc){
        $sql ="insert into tin_tuc values(?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$ten_tin_tuc,$hinh_anh,$noi_dung,$ngay_tao,$trang_thai,$id_danh_muc_tin_tuc));
    }
    public function edit_tintuc($id,$ten_tin_tuc,$hinh_anh,$noi_dung,$trang_thai,$id_danh_muc_tin_tuc){
        $sql="update tin_tuc set ten_tin_tuc=?,hinh_anh=?,noi_dung=?,trang_thai=?,id_danh_muc_tin_tuc=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten_tin_tuc,$hinh_anh,$noi_dung,$trang_thai,$id_danh_muc_tin_tuc,$id));
    }
    public function delete_tin_tuc($id){
        $sql = "delete from tin_tuc where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}