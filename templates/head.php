<!DOCTYPE html>
<html lang="en-US">


<!-- Mirrored from education-html.themerex.net/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Jun 2018 19:27:13 GMT -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/x-icon" href="public/layout/images/favicon.ico" />
    <title>Homepage | Education Center</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=4.3.1" type="text/css" media="all" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,700,700italic&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" type="text/css" media="all" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister:400&amp;subset=latin" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/fontello/css/fontello.css" type="text/css" media="all" />

    <link rel="stylesheet" href="public/layout/js/rs-plugin/settings.css" type="text/css" media="all" />

    <link rel="stylesheet" href="public/layout/css/woocommerce/woocommerce-layout.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/woocommerce/woocommerce-smallscreen.css" type="text/css" media="only screen and (max-width: 768px)" />
    <link rel="stylesheet" href="public/layout/css/woocommerce/woocommerce.css" type="text/css" media="all" />

    <link rel="stylesheet" href="public/layout/css/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/shortcodes.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/core.animation.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/tribe-style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/skins/skin.css" type="text/css" media="all" />

    <link rel="stylesheet" href="public/layout/css/core.portfolio.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/js/mediaelement/mediaelementplayer.min.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/js/mediaelement/wp-mediaelement.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/js/core.customizer/front.customizer.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/js/core.messages/core.messages.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/js/swiper/idangerous.swiper.min.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/responsive.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/skins/skin-responsive.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/slider-style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="public/layout/css/custom-style.css" type="text/css" media="all" />
</head>