<?php
include ("models/m_read_id.php");
include ("models/m_dang_ky.php");
class c_dang_ky{
    public function indes(){
        if(isset($_GET['id'])){
            $id_show=$_GET['id'];
            $show = new m_read_id();
            $show_kh = $show->read_id_show_dang_ky($id_show);
            $show_km =$show->read_all_khuyen_mai();
        }
        if (isset($_POST['btn_save'])){
            $id=null;
            $ho_ten= $_POST['ho_ten'];
            $sdt = $_POST["so_dien_thoai"];
            $email = $_POST["email"];
//            date_default_timezone_set("Asia/Ha_Noi");
            $ndk = date("Y/m/d");
            $id_km=$_POST["id_khuyen_mai"];
            $show_idkm=$show->read_show_id_khuyen_mai($id_km);
            $khuyen_mai=$show_idkm->phan_tram_giam_gia;
            $gt= $show_kh->hoc_phi;
            $gia_tien= $gt-(($gt*$khuyen_mai)/100);
            $id_lp = $id_show;
            $trang_thai=0;
            $add_dk= new m_dang_ky();
            $add_dky= $add_dk->add_dangky($id,$ho_ten,$sdt,$email,$ndk,$gia_tien,$id_km,$id_lp,$trang_thai);
            $this->sendMail($ho_ten,$sdt,$email,$id_lp,$id_km,$gia_tien);
            if ($add_dky) {

                echo "<script>window.location='xac_nhan.php'</script>";

            }
        }
        include("views/dang_ky/v_dang_ky_php.php");
    }
    function sendMail($ho_ten,$sdt,$email,$id_lp,$id_km,$gia_tien)
    {
        require_once("libs/Helper.php");
        $show = new m_read_id();
        $show_kh = $show->read_id_show_dang_ky($id_lp);
        $show_idkm=$show->read_show_id_khuyen_mai($id_km);
        $tieu_de = "Liên hệ xác nhân thông tin học viên";
        $xhtml = "<p><strong>Tên khóa học: ".$show_kh->ten_khoa_hoc."</strong></p>";
        $xhtml .= "<p><strong>Trạng thái: Chưa thanh toán</strong></p>";

        $xhtml .= '<table style="width: 100%;border-collapse: collapse"></table>';
        $xhtml .= '<tr style="background-color: #0f81bb">';
        $xhtml .= '<th style="border: 1px solid #00FFFF;text-align: center; padding: 8px">Tên học viên</th>';
        $xhtml .= '<th style="border: 1px solid #00FFFF;text-align: center; padding: 8px">Số điện thoại</th>';
        $xhtml .= '<th style="border: 1px solid #00FFFF;text-align: center; padding: 8px">Lớp</th>';
        $xhtml .= '<th style="border: 1px solid #00FFFF;text-align: center; padding: 8px">Thời gian</th>';
        $xhtml .= '<th style="border: 1px solid #00FFFF;text-align: center; padding: 8px">Ngày khai giảng</th>';
        $xhtml .= '<th style="border: 1px solid #00FFFF;text-align: center; padding: 8px">Địa điểm học</th>';
        $xhtml .= '<th style="border: 1px solid #00FFFF;text-align: center; padding: 8px">Học phí</th>';
        $xhtml .= '<th style="border: 1px solid #00FFFF;text-align: center; padding: 8px">Phần trăm giảm giá</th></tr>';

        $xhtml .= '<tr>';
        $xhtml .= "<td style='border: 1px solid #00FFFF; text-align: center; padding: 8px'>". $ho_ten ."</td>";
        $xhtml .= "<td style='border: 1px solid #00FFFF; text-align: center; padding: 8px'>". $sdt ."</td>";
        $xhtml .= "<td style='border: 1px solid #00FFFF; text-align: center; padding: 8px'>". $show_kh->ten_lop_hoc ."</td>";
        $xhtml .= "<td style='border: 1px solid #00FFFF; text-align: center; padding: 8px'>". $show_kh->ca_hoc ."</td>";
        $xhtml .= "<td style='border: 1px solid #00FFFF; text-align: center; padding: 8px'>". $show_kh->thoi_gian_bat_dau."</td>";
        $xhtml .= "<td style='border: 1px solid #00FFFF; text-align: center; padding: 8px'>". $show_kh->dia_diem_hoc ."</td>";
        $xhtml .= "<td style='border: 1px solid #00FFFF; text-align: center; padding: 8px'>". $show_kh->hoc_phi ."</td>";
        $xhtml .= "<td style='border: 1px solid #00FFFF; text-align: center; padding: 8px'>". $show_idkm->phan_tram_giam_gia.'%'."</td></tr></table>";
        $xhtml .= "<p style='text-align: right'><strong>Tổng tiền cần đóng là: ".number_format($gia_tien)."</strong></p>";
        $noi_dung_mail = "<b>Từ: </b>ABCD<p/><b>Email:</b>ABCD<p/>Đăng kí khóa học .$show_kh->ten_khoa_hoc.thành công!";

        $kq=Helper::Gui_mail_lien_he($tieu_de,$xhtml,$email);
    }
}