<?php
include ("models/m_read_id.php");
class c_khoa_hoc{
    public  function index(){
        if(isset($_GET['id_dmkh'])){
            $id = $_GET['id_dmkh'];
            $show = new m_read_id();
            $lh = $show->read_all_lien_he();
            $dm = $show->read_all_danh_muc_khoa_hoc();
            $km = $show->read_all_danh_nuc_tin_tuc();
            $show_kh = $show->read_user_khoa_hoc($id);
            $show_dmkh = $show->read_id_danh_muc($id);
        }
        $view = "views/khoa_hoc/v_khoa_hoc.php";
        include('templates/layout.php');
    }
}
