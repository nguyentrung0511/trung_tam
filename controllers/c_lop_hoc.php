<?php
include ("models/m_read_id.php");
class c_lop_hoc{
    public function index(){
        if(isset($_GET['id_kh'])) {
            $id = $_GET['id_kh'];
            $show = new m_read_id();
            $lh = $show->read_all_lien_he();
            $dm = $show->read_all_danh_muc_khoa_hoc();
            $km = $show->read_all_danh_nuc_tin_tuc();
            $show_kh = $show->read_user_id_khoa_hoc($id);
            $show_lp= $show->read_user_lop_hoc($id);
        }
        $view = "views/lop_hoc/v_lop_hoc.php";
        include('templates/layout.php');
    }
}