<?php
include ("database.php");
class  m_read_user_all extends database{
    public function read_tieu_de(){
        $sql = "select * from tieu_de";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_danh_muc_khoa_hoc(){
        $sql = "select * from danh_muc_khoa_hoc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_khuyen_mai(){
        $sql = "select * from khuyen_mai";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_danh_nuc_tin_tuc(){
        $sql = "select * from danh_muc_tin_tuc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_lien_he(){
        $sql = "select * from lien_he";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

}