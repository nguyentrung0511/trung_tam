<?php
require_once ("database.php");
class m_dang_ky extends database
{
    public function add_dangky($id,$ho_ten,$so_dien_thoai,$email,$ngay_dk,$gia_tien,$id_khuyen_mai,$id_lop,$trang_thai)
    {
        $sql ="insert into dang_ky values(?,?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$ho_ten,$so_dien_thoai,$email,$ngay_dk,$gia_tien,$id_khuyen_mai,$id_lop,$trang_thai));
    }
}